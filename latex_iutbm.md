---
title: Beamer IUTBM
author: Romain GONCALVES
date: 2019
theme: iutbm
fontfamily: noto-sans
fontsize: 12pt
---


## En quoi consiste ce métier ?

*Les qualités requises et le travail attendu dépendent de comment est structuré le studio*

- Le développement du moteur de jeux / outils de dévelopement
- Le développement de l'infrastructure réseau
- Le développement des éléments de jeu (joueurs, véhicules, ...)
- Le développement de l'intelligence artificielle

## Quels compétences ?

- Maîtrise des outils de développement
- Sens du travail d’équipe 
- Sens du relationnel 
- Etre ouvert aux critiques
- Autonomie 
- Créativité 

> Et bien sur : Disponibilité et Rigueur 

## Quels sont les études nécessaires ?

1. Etudes
	- DUT informatique
	- License informatique
	- Diplôme d'ingénieur
	- Ecoles spécialisées
2. Expérience
	- Professionelle
	- Création jeux vidéo
	- Programmation

## Comment accéder à cet emploi ?

En moyenne, *2 employeurs différents* sur les 5 dernières années :

- Freelance
- CDI

Différents services peuvent être proposés par l'employeur :

- Aide à la relocation
- Assurance vie

## Quels sont les responsabilités ?

Avec le *développeur en chef* :

- **établir** un cahier des charges
- **concevoir** les interactions joueurs $\leftrightarrow$ réseau $\leftrightarrow$ jeu
- **programmer** les interactions / les différentes classes
- **tester** continuellement
- **fixer** les bugs / **réviser** le code

On peut également travailler sur *d'autres domaines* :

- **profiler** les performances
- **optimiser** le code
- **modifier** le moteur de jeu

## Les évolutions possibles

Le domaine du jeux vidéo permet de naviguer entre différents corps de métiers :

- chef de projet
- développeur en chef / (lead developer)
- responsable des builds
- game designer
- technical artist
- level designer

## Les outils de dévelopement

Outils 					
----------------		------------------
Visual Studio			*
Unreal Engine 4 		C++ et Blueprints
Unity					C#
Godot					C#, C++, VisualScripting, Lua, ...
Contrôle de Versions	Perforce, Git, SVN, ...

## La rémunération

- Début de carrière : 20 000 à 30 000 euros NET   /ans
- Fin de carrière : 40 000 euros NET   /ans

## Conclusion

#### Sources :

[https://orientation.com](https://www.orientation.com/metiers/developpeur-jeux-video.html)

[https://www.gamasutra.com](https://gamasutra.com/view/news/340662/Managing_your_game_dev_career_from_early_to_late_stages.php)
