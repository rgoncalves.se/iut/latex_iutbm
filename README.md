![logo](logo.png)

Fichiers Latex non-officiels pour l'IUT d'informatique de Belfort-Montbéliard.

# Crédits

> Tom Pollard et al. (2016). Template for writing a PhD thesis in Markdown. 
> Zenodo. http://dx.doi.org/10.5281/zenodo.58490
> https://github.com/tompollard/phd_thesis_markdown

> Jérome Belleman
> https://github.com/jeromebelleman/beamer-cern
