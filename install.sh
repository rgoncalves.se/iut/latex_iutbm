#!/usr/bin/env bash

BEAMERFOLDER=/usr/share/texmf-dist/tex/latex/beamer/
FIGUREFOLDER=${BEAMERFOLDER}/iutbmfigures/

ln -sfn ${PWD}/templates/iutbm_slides/*.sty ${BEAMERFOLDER}
mkdir ${FIGUREFOLDER}
ln -sfn ${PWD}/templates/iutbm_slides/*.png ${FIGUREFOLDER}
